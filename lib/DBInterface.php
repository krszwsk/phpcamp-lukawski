<?php
interface iDB {
/**
* @param string $host Adres DB
* @param string $login Login użytkownika
* @param string $password Hasło użytkownika
* @param string $dbName Nazwa bazy danych
*/
public function __construct($host, $login, $password, $dbName);

/**
* Wykonuje zapytanie, zapisuje uchwyt
* @param string $query Zapytanie do wykonania
* @return bool Czy się udało wykonać zapytanie czy nie
*/
public function query($query);

/**
* Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
* @return int
*/
public function getAffectedRows();

/**
* Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
* @return mixed
*/
public function getRow();

/**
* Zwraca wszystkie wiersze z wyniku ostatniego zapytania
* @return mixed
*/
public function getAllRows();
}