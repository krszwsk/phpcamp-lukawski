<?php

class Emerytura /*ProductBase*/ {
  /*protected $id;
  protected $name;
  protected $price;
  protected $currency;
  protected $description;
  protected $images;
  protected $manufacturer;
  protected $categories;  

  public function setId($id) {
    $this->id = $id;
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function setPrice($price) {
    $this->price = $price;
  }

  public function setCurrency($currency) {
    $this->currency = $currency;
  }

  public function setDescription($descr) {
    $this->description = $descr;
  }

  public function setImages($images) {
    $this->images = $images;
  }

  public function setManufacturer($manuf) {
    $this->manufacturer = $manuf;
  }

  public function setCategories($categories) {
    $this->categories = $categories;
  }

  public function getId() {
    return $this->id;
  }

  public function getName() {
    return $this->name;
  }

  public function getPrice() {
    return $this->price;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getImages() {
    return $this->images;
  }

  public function getManufacturer() {
    return $this->manufacturer;
  }

  public function getCategories() {
    return $this->categories;
  } */

  public function __set($name, $value) {
    $this->$name = $value;
  }

  public function __get($name) {
    return $this->$name;
  }

  public function __call($name, $arguments) {
    $methodName = substr($name, 0, 3);
    $propertyName = lcfirst(substr($name, 3));

    if ($methodName === 'set') {
      $this->$propertyName = $arguments[0];
    } else if ($methodName === 'get') {
      return $this->$propertyName;
    }
  }
}