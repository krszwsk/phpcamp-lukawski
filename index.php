<?php
  /*
  header("Content-type: text/xml");
  $t = microtime(true);
  require_once './lib/DBClass.php';
  require_once './lib/PhysicalProductClass.php';
  require_once './lib/ProductBaseClass.php';
 
  //$DB = new DB('127.0.0.1', 'root', '', 'phpcamp_klukawski');
  
  //echo (memory_get_peak_usage(true) / 1024).'<br>';

  $products;

  for ($i = 1; $i <= 2; $i++) {
    $product = new Emerytura();
    $product -> setId($i);
    $product -> setName('Kropla Beskidu');
    $product -> setPrice(120);
    $product -> setCurrency('PLN');
    $product -> quantity = 10;
    $product -> setDescription('Woda');
    $product -> setImages('img');
    $product -> setManufacturer('Disney');
    $product -> setCategories([1, 2]);
    $product -> weight = 500;
    
    $products [] = $product;
  }

  echo var_dump($products);
  //echo (memory_get_peak_usage(true) / 1024).'<br>';
  //echo (microtime(true) - $t);
 */

class ProductAPI {
  private $products = [
    ['id' => 0, 'name' => 'Bamity', 'price' => 57.01],
    ['id' => 1, 'name' => 'Name2', 'price' => 57.01],
    ['id' => 2, 'name' => 'Name3', 'price' => 57.01],
    ['id' => 3, 'name' => 'Name4', 'price' => 57.01],
    ['id' => 4, 'name' => 'Name5', 'price' => 57.01],
    ['id' => 5, 'name' => 'Name6', 'price' => 57.01],
    ['id' => 6, 'name' => 'Name7', 'price' => 57.01]
  ];

  public function checkProduct($name) {
    if (empty($name)) {
      return $this->products;
    }

    $product;
    foreach ($products as $value) {
      foreach ($value as $key => $value2) {
        if ($key !== 'name') continue;

        if ($value2 === $name) {
          $product = $value;
          break;
        }
      }
    }

    return $product;
  }

  public function addProduct($name, $price) {
    $productsQuantity = count($this->products);
    $product = ['id' => $productsQuantity, 'name' => $name, 'price' => $price];

    array_push($this->products, $product);
    return $this->products;
  }

  public function removeProduct($id) {
    //array_splice($this->products, $id, 1);
    unset($this->products[$id]);
    return $this->products;
  }
}

$options = array ('uri' => 'http://localhost/');
$server = new SoapServer(NULL, $options);
$server->setClass('ProductAPI');
$server->handle();

/*
if (empty($_GET['action'])) {
  die('No action specified');
}
  $action = $_GET['action'];

  switch($action) {
    case 'checkProduct':
      if (empty($_GET['name'])) {
        $xml = new SimpleXmlElement('<?xml version="1.0" encoding="UTF-8"?><products/>');
        foreach ($products as $value) {
          $child = $xml->addChild('product');
          foreach ($value as $key => $value) {
            $child->addChild($key, $value);
          }
        }

        echo $xml->asXML();

        //echo json_encode($products);
        die;
      }
      $name = $_GET['name'];
      $product;
      foreach ($products as $value) {
        foreach ($value as $key => $value2) {
          if ($key !== 'name') continue;

          if ($value2 === $name) {
            $product = $value;
            break;
          }
        }
      }
      $xml = new SimpleXmlElement('<?xml version="1.0" encoding="UTF-8"?><product/>');
      foreach ($product as $key => $value) {
        $xml->addChild($key, $value);
      }

      echo $xml->asXML();
      //echo json_encode($product);
      die;
      break;  
    case 'addProduct':
      if (empty($_GET['name']) || empty($_GET['price'])) die('Either name and price is required');

      $productsQuantity = count($products);
      $product = ['id' => $productsQuantity, 'name' => $_GET['name'], 'price' => (float)$_GET['price']];

      array_push($products, $product);
      echo json_encode($products);
      die;
      break;
    case 'removeProduct':
      if (empty($_GET['id'])) die('ID is required');
      $id = (int)$_GET['id'];
      array_splice($products, $id, 1);
      echo json_encode($products);
      die;
      break;
  }
*/
